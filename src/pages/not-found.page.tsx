import React from 'react';

import { Error } from '../components/error';

export const NotFoundPage: React.FC = () => (
  <Error title="Страница не найдена" needButton />
);
