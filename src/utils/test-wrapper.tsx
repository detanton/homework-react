import React from 'react';
import { RouterProvider, createMemoryRouter } from 'react-router-dom';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import { render } from '@testing-library/react';

import { persistor, store } from '../storage/store';
import { routerConfig } from '../routes';
import { setUser } from '../storage/slices/user/user';

interface TestWrapperProps {
  initialRouterEntires: Array<string>;
}

const testToken =
  'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2NTI5YmVkYmNkMTYyY2IwZTQzZjExMGUiLCJncm91cCI6InJhLTIiLCJpYXQiOjE2OTg4ODMwMjYsImV4cCI6MTczMDQxOTAyNn0.6FGad2L8MEbQKsjpxxYKNYmNfHiMbMkLvRZ2jedLiDg';

export const WrapperProviderRender = ({
  children
}: {
  children: React.ReactElement;
}) => (
  <Provider store={store}>
    <PersistGate loading={null} persistor={persistor}>
      {children}
    </PersistGate>
  </Provider>
);

export const TestWrapperProvider = ({
  initialRouterEntires
}: TestWrapperProps) => {
  const testsRouter = createMemoryRouter(routerConfig, {
    initialEntries: initialRouterEntires
  });

  store.dispatch(setUser({ token: testToken }));

  render(
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <RouterProvider router={testsRouter} />
      </PersistGate>
    </Provider>
  );

  return testsRouter;
};

export { render } from '@testing-library/react';
