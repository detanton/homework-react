export interface CounterProps {
  initialValue: number;
  max: number;
  onClickMinus?: (value: number) => void;
  onClickPlus?: (value: number) => void;
}
