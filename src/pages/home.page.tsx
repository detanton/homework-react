import React from 'react';
import { useNavigate } from 'react-router-dom';

import { accessTokenSelector } from '../storage/slices/user/selectors';
import { useAppSelector } from '../hooks/use-app-selector';
import { Header } from '../components/typography/header';

export const HomePage: React.FC = () => {
  const navigate = useNavigate();
  const accessToken = useAppSelector(accessTokenSelector);

  React.useLayoutEffect(() => {
    if (!accessToken) {
      navigate('/signin');
    }
  }, [accessToken, navigate]);

  return <Header as="h1" title="Home Page" />;
};
