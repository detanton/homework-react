const signInError = /неправильные почта или пароль/i;

describe('Авторизация', () => {
  beforeEach(() => {
    cy.visit('/signin');
  });

  it('Логинимся с неверным email', () => {
    cy.get('input[name="email"]').type('det@gmail.com');
    cy.get('input[name="password"]').type('I71&fyh5');
    cy.get('button[type="submit"]').click();
    cy.get('div#error').contains(signInError);
  });

  it('Логинимся с неверным password', () => {
    cy.get('input[name="email"]').type('detanton@gmail.com');
    cy.get('input[name="password"]').type('I71&fyh6');
    cy.get('button[type="submit"]').click();
    cy.get('div#error').contains(signInError);
  });

  it('Указываем верные данные', () => {
    cy.get('input[name="email"]').type('detanton@gmail.com');
    cy.get('input[name="password"]').type('I71&fyh5');
    cy.get('button[type="submit"]').click();
  });

  it('Разлогиниваемся', () => {
    cy.wait(1000);
    cy.visit('/profile');
    cy.get('button[type="button"]').contains(/выйти/i).click();
  });
});
