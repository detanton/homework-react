export interface Profile {
  _id: string;
  name: string;
  about: string;
  avatar: string;
  email: string;
  token: string;
}
