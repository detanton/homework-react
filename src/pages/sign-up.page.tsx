import React from 'react';

import { RegisterForm } from '../components/forms/register-form';

export const SignUpPage: React.FC = () => <RegisterForm />;
