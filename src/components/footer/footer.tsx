import React from 'react';

import { BottomNavigation } from '../bottom-navigation';
import { ContactInfo } from '../contact-info';
import { Logo } from '../logo';

export const navElements = [
  { title: 'Каталог', url: '/products' },
  { title: 'Акции', url: '/promotions' },
  { title: 'Новости', url: '/news' },
  { title: 'Отзывы', url: '/reviews' }
];

export const additionalNavElements = [
  { title: 'Оплата и доставка', url: '/payment' },
  { title: 'Часто спрашивают', url: '/faq' },
  { title: 'Обратная связь', url: '/feedback' },
  { title: 'Контакты', url: '/contacts' }
];

export const socialMedia = [
  { type: 'telegram', url: 'https://telegram.org/?ysclid=lnascgru87847328782' },
  { type: 'viber', url: 'https://www.viber.com/ru/' },
  { type: 'vk', url: 'https://vk.com' },
  { type: 'instagram', url: 'https://instagram.com' },
  { type: 'whatsapp', url: 'https://www.whatsapp.com/' }
];

export const Footer: React.FC = () => (
  <footer className="bg-yellow-300 w-full py-10">
    <section className="max-w-[960px] flex justify-between mx-auto h-full">
      <div className="flex flex-col justify-between">
        <Logo />
        <p className="text-[9px]">© «Интернет-магазин DogFood.ru»</p>
      </div>
      <BottomNavigation navElements={navElements} />
      <BottomNavigation navElements={additionalNavElements} />
      <ContactInfo
        socialMedia={socialMedia}
        title="Мы на связи"
        email="dogfood.ru@gmail.com"
        phone="8 (999) 00-00-00"
      />
    </section>
  </footer>
);
