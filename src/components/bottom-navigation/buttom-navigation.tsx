import React from 'react';
import { Link, useLocation } from 'react-router-dom';

import type { BottomNavigationProps } from './types';

export const BottomNavigation: React.FC<BottomNavigationProps> = ({
  navElements
}) => {
  const { pathname } = useLocation();

  return (
    <ul className="flex flex-col justify-between h-full">
      {navElements.map((element) => (
        <li key={element.title} className="text-xs font-semibold">
          <Link
            to={element.url}
            state={{ url: pathname }}
            data-testid={element.url.replace('/', '')}
            className="transition-colors ease-in-out duration-300 hover:text-slate-400"
          >
            {element.title}
          </Link>
        </li>
      ))}
    </ul>
  );
};
