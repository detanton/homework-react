import { createApi } from '@reduxjs/toolkit/query/react';

import { setUser } from '../slices/user/user';
import { customBaseQuery } from '../../utils/custom-base-query';

import type { ApiError } from './types';

export const authApi = createApi({
  reducerPath: 'authApi',
  baseQuery: customBaseQuery,
  endpoints: (builder) => ({
    signUp: builder.mutation({
      query: (data) => ({
        url: '/signup',
        method: 'POST',
        body: data
      })
    }),
    signIn: builder.mutation({
      query: (data) => ({
        url: '/signin',
        method: 'POST',
        body: data
      }),
      async onQueryStarted(args, { dispatch, queryFulfilled }) {
        try {
          const { data } = await queryFulfilled;
          dispatch(setUser(data));
        } catch (error) {
          dispatch(setUser({ error: (error as ApiError)?.data?.message }));
        }
      }
    })
  })
});

export const { useSignUpMutation, useSignInMutation } = authApi;
