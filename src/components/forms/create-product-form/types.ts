export type CreateProductFormInputs = {
  name: string;
  price: number;
  discount?: number;
  stock?: number;
  wight?: string;
  description: string;
  pictures: string;
};
