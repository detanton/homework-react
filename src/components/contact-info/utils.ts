import type { IconDefinition } from '@fortawesome/free-brands-svg-icons';
import {
  faVk,
  faTelegram,
  faInstagram,
  faWhatsapp,
  faViber
} from '@fortawesome/free-brands-svg-icons';

const socialIcons: Record<string, IconDefinition> = {
  vk: faVk,
  telegram: faTelegram,
  instagram: faInstagram,
  whatsapp: faWhatsapp,
  viber: faViber
};

export const getSocialIcon = (type: string) => socialIcons[type];
