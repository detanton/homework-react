import '@testing-library/jest-dom';
import { describe, expect, it } from '@jest/globals';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import type { createMemoryRouter } from 'react-router-dom';

import { TestWrapperProvider } from '../../../utils/test-wrapper';

describe('Тестирование компонента Header', () => {
  let router: ReturnType<typeof createMemoryRouter>;

  beforeEach(() => {
    router = TestWrapperProvider({ initialRouterEntires: ['/'] });
  });

  it('Отображается логотип', () => {
    // Поскольку у нас 2 одинаковых изображения логотипа: в header и footer, выбираем первое
    const logoImage = screen.getAllByRole('img', { name: 'DogFood' }).at(0);
    expect(logoImage?.getAttribute('alt')).toContain('DogFood');
  });

  it('Отображается ссылка Избранное', () => {
    const favoriteLink = screen.getByTestId('favorites');
    expect(favoriteLink).toBeDefined();
  });

  it('Отображается ссылка Корзина', () => {
    const cartLink = screen.getByTestId('cart');
    expect(cartLink).toBeDefined();
  });

  it('Отображается ссылка Профиль', () => {
    const profileLink = screen.getByTestId('profile');
    expect(profileLink).toBeDefined();
  });

  it('Переход на страницу Корзина', async () => {
    const favoriteProductsLink = screen.getByTestId('cart');

    await userEvent.click(favoriteProductsLink);

    expect(router?.state.location.pathname).toEqual('/cart');
  });
});
