import { defineConfig } from 'cypress';

export default defineConfig({
  component: {
    devServer: {
      framework: 'react',
      bundler: 'webpack'
    }
  },

  e2e: {
    video: false,
    screenshotOnRunFailure: false,
    supportFile: false,
    baseUrl: 'http://localhost:3000'
  }
});
