export interface FieldProps {
  label: string;
  name: string;
  type: 'text' | 'email' | 'password' | 'number' | 'textarea';
  placeholder: string;
  maxLength?: number;
  error?: string;
  control?: unknown;
  min?: number;
  max?: number;
}
