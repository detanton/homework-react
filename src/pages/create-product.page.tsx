import React from 'react';

import { withProtection } from '../HOCs/with-protection';
import { CreateProductForm } from '../components/forms/create-product-form';

export const CreateProductPage: React.FC = withProtection(() => (
  <CreateProductForm />
));
