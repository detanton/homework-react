import React from 'react';

import { withProtection } from '../HOCs/with-protection';
import { TopNavigation } from '../components/top-navigation';
import { ChangeProfileForm } from '../components/forms/change-profile-form';

export const ProfileEditPage: React.FC = withProtection(() => (
  <>
    <TopNavigation />
    <ChangeProfileForm />
  </>
));
