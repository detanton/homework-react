import type { ReactElement } from 'react';

export interface ButtonProps {
  children?: ReactElement;
  title: string;
  type?: 'submit' | 'submit' | 'button';
  className?: string;
  icon?: React.ReactElement;
  iconLeftPosition?: boolean;
  iconRightPosition?: boolean;
  transparent?: boolean;
  fullWidth?: boolean;
  disabled?: boolean;
  id?: string;
  form?: string;
  onClick?: () => void;
}
