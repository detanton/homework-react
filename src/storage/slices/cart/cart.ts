import { createSlice } from '@reduxjs/toolkit';

import type { InitialStateCart } from './types';

const initialState: InitialStateCart = {
  total: 0,
  productsPrice: 0,
  productsDiscount: 0,
  summary: 0,
  cartProducts: []
};

const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addToCart: (state, action) => {
      state.cartProducts.push(action.payload);
      state.total = state.cartProducts.length;
    },
    removeFromCart: (state, action) => {
      const index = state.cartProducts?.findIndex(
        (item) => item.id === action.payload.id
      );
      state.cartProducts.splice(index, 1);
      state.total = state.cartProducts.length;
    },
    changeCountProductToCart: (state, action) => {
      const index = state.cartProducts.findIndex(
        (product) => product.id === action.payload.id
      );

      if (index !== -1) {
        state.cartProducts[index].count = action.payload.count;
      }
    },
    clearCart: (state) => {
      (state.total = 0),
        (state.productsPrice = 0),
        (state.productsDiscount = 0),
        (state.summary = 0),
        (state.cartProducts = []);
    }
  }
});

export const sliceName = 'cart';

export const {
  addToCart,
  removeFromCart,
  changeCountProductToCart,
  clearCart
} = cartSlice.actions;

export default cartSlice.reducer;
