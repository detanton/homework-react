import React from 'react';

import { withProtection } from '../HOCs/with-protection';
import { TopNavigation } from '../components/top-navigation';
import { SingleProduct } from '../components/single-product';

export const ProductPage: React.FC = withProtection(() => (
  <>
    <TopNavigation />
    <SingleProduct />
  </>
));
