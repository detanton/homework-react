import React from 'react';
import { useDispatch } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart as faHeartSolid } from '@fortawesome/free-regular-svg-icons';
import { faHeart } from '@fortawesome/free-solid-svg-icons';
import { Link, useLocation } from 'react-router-dom';

import { userSelector } from '../../storage/slices/user/selectors';
import { Discount } from '../discount';
import { Price } from '../price/price';
import { Button } from '../button';
import { useToggleFavoriteMutation } from '../../storage/api/products';
import { addToCart } from '../../storage/slices/cart/cart';
import { useAppSelector } from '../../hooks/use-app-selector';
import { cartSelectorProduct } from '../../storage/slices/cart/selectors';
import { toggleIsFavorite } from '../../storage/slices/products/products';

import type { CardProps } from './types';

export const Card: React.FC<CardProps> = ({
  _id,
  name,
  pictures,
  price,
  discount,
  wight,
  likes,
  stock
}) => {
  const dispatch = useDispatch();
  const { pathname } = useLocation();
  const [toogleFavorite] = useToggleFavoriteMutation();
  const { data: user } = useAppSelector(userSelector);
  const { cartProduct } = useAppSelector((state) =>
    cartSelectorProduct(state, _id)
  );
  const [isFavorite, setFavorite] = React.useState(
    likes.includes(user?._id || '')
  );

  const disabledButton = cartProduct?.id === _id;

  const handleClickAddCart = React.useCallback(() => {
    dispatch(
      addToCart({
        id: _id,
        count: 1,
        price,
        discount,
        pictures,
        wight,
        name,
        stock
      })
    );
  }, [_id, discount, dispatch, name, pictures, price, wight, stock]);

  const handleClickChangeFavorite = React.useCallback(() => {
    setFavorite(!isFavorite);
    toogleFavorite({ id: _id, toggle: isFavorite });
    dispatch(toggleIsFavorite(isFavorite));
  }, [_id, dispatch, isFavorite, toogleFavorite]);

  return (
    <div className="relative h-[359px]">
      <img
        src={pictures}
        alt={name}
        loading="lazy"
        className="w-48 h-48 block text-center mx-auto object-contain"
      />
      <div className="py-4">
        <button
          title="favorite"
          data-testid="favorite"
          className="absolute top-0 right-0"
          onClick={handleClickChangeFavorite}
        >
          <FontAwesomeIcon
            icon={isFavorite ? faHeart : faHeartSolid}
            className={`text-xl ${isFavorite && 'text-red-500'}`}
          />
        </button>
        <Discount discount={discount} />
        <Price discount={discount} price={price} size="xl" />
        <p className="text-xs text-gray-400">{wight}</p>
        <Link to={`/products/${_id}`} state={{ url: pathname }} title={name}>
          <p className="font-semibold truncate hover:text-yellow-400 transition-colors ease-in-out duration-300">
            {name}
          </p>
        </Link>
      </div>
      <Button
        title="В корзину"
        onClick={handleClickAddCart}
        className="absolute bottom-0 left-0"
        disabled={disabledButton}
      />
    </div>
  );
};
