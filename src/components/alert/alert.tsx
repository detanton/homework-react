import React from 'react';

import type { AlertProps } from './types';

export const Alert: React.FC<AlertProps> = ({
  title,
  icon,
  description,
  className
}) => (
  <div className={`bg-gray-100 rounded-lg p-3 ${className}`}>
    <h5 className="font-bold">
      {icon && <span className="mr-3">{icon}</span>}
      {title}
    </h5>
    {description && (
      <ul className="ml-8">
        {description.map((item) => (
          <li key={item} className="my-2 text-sm">
            {item}
          </li>
        ))}
      </ul>
    )}
  </div>
);
