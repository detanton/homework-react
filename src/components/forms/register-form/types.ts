export interface FieldProps {
  label: string;
  name: string;
  type: 'text' | 'email' | 'password';
  placeholder: string;
  maxLength?: number;
  error?: string;
  control?: unknown;
}

export type Inputs = {
  email: string;
  password: string;
  group: string;
};

export interface ServerError {
  data: {
    message: string;
  };
}
