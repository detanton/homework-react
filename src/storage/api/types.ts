import type { Product } from '../../components/card/types';

export interface ServerError {
  message: string;
}

export interface ApiError {
  status: number;
  data: {
    message: string;
  };
}

export interface Products {
  total: number;
  products: Array<Product>;
}
