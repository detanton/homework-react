import React from 'react';
import type { SubmitHandler } from 'react-hook-form';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useLocation, useNavigate } from 'react-router-dom';
import * as yup from 'yup';

import { Button } from '../../button';
import type { ApiError } from '../../../storage/api/types';
import { Header } from '../../typography/header';
import { useCreateProductMutation } from '../../../storage/api/products';
import { MESSAGES } from '../../../constants';

import { Field } from './field';
import type { CreateProductFormInputs } from './types';
import { BUTTONS, FIELDS } from './constants';

const schema = yup.object({
  name: yup.string().required(MESSAGES.REQUIRED),
  price: yup.number().required(MESSAGES.REQUIRED),
  discount: yup.number(),
  stock: yup.number(),
  wight: yup.string(),
  description: yup.string().required(MESSAGES.REQUIRED),
  pictures: yup.string().required(MESSAGES.REQUIRED)
});

export const CreateProductForm: React.FC = () => {
  const [
    createProduct,
    { error: errorCreateProduct, isLoading, data: product }
  ] = useCreateProductMutation();
  const navigate = useNavigate();
  const { pathname } = useLocation();

  const {
    handleSubmit,
    control,
    formState: { errors }
  } = useForm<CreateProductFormInputs>({
    resolver: yupResolver(schema),
    defaultValues: {
      name: '',
      price: 0,
      discount: 0,
      stock: 0,
      wight: '',
      description: '',
      pictures: ''
    }
  });

  const onSubmit: SubmitHandler<CreateProductFormInputs> = React.useCallback(
    async (values) => {
      await createProduct(values);
    },
    [createProduct]
  );

  React.useEffect(() => {
    if (product) {
      navigate(`/products/${product._id}`);
    }
  }, [navigate, pathname, product]);

  return (
    <section className="flex items-center justify-center flex-col h-full">
      {errorCreateProduct && (
        <div
          data-testid="api-error"
          className="my-2 bg-rose-400 p-4 w-full text-white rounded-lg"
        >
          {(errorCreateProduct as ApiError)?.data?.message}
        </div>
      )}
      <Header title="Добавление товара в каталог" as="h1" className="mt-6" />
      <form onSubmit={handleSubmit(onSubmit)} className="mt-6 w-[650px]">
        <Field
          control={control}
          name="name"
          type="text"
          placeholder={FIELDS.HEADER}
          label={FIELDS.HEADER}
          error={errors.name?.message}
        />
        <Field
          control={control}
          name="description"
          type="textarea"
          placeholder={FIELDS.DESCRIPTION}
          label={FIELDS.DESCRIPTION}
          error={errors.description?.message}
        />
        <Field
          control={control}
          name="pictures"
          type="text"
          placeholder={FIELDS.PICTURES}
          label={FIELDS.PICTURES}
          error={errors.pictures?.message}
        />
        <Field
          control={control}
          name="wight"
          type="text"
          placeholder={FIELDS.WEIGHT}
          label={FIELDS.WEIGHT}
          error={errors.wight?.message}
        />
        <Field
          control={control}
          name="stock"
          type="number"
          placeholder={FIELDS.STOCK}
          label={FIELDS.STOCK}
          min={0}
          error={errors.stock?.message}
        />
        <Field
          control={control}
          name="price"
          type="number"
          placeholder={FIELDS.PRICE}
          label={FIELDS.PRICE}
          min={0}
          error={errors.price?.message}
        />
        <Field
          control={control}
          name="discount"
          type="number"
          placeholder={FIELDS.DISCOUNT}
          label={FIELDS.DISCOUNT}
          min={0}
          error={errors.discount?.message}
        />
        <Button
          title={BUTTONS.SUBMIT}
          type="submit"
          fullWidth
          className="mt-10 mb-12"
          disabled={isLoading}
        />
      </form>
    </section>
  );
};
