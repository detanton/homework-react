import React from 'react';

import { Loader } from '../loader';
import { MESSAGES } from '../../constants';

import type { InfiniteScrollProps } from './types';

export const InfiniteScroll: React.FC<InfiniteScrollProps> = ({
  action,
  isLoading,
  isEndOfList
}) => {
  const ref = React.useRef<HTMLDivElement>(null);

  React.useLayoutEffect(() => {
    let observer: IntersectionObserver | undefined = undefined;

    if (!isEndOfList) {
      const options: IntersectionObserverInit = {
        threshold: 0.5
      };
      const callback: IntersectionObserverCallback = (entries) => {
        if (entries[0].isIntersecting) {
          action();
        }
      };

      observer = new IntersectionObserver(callback, options);
      ref.current && observer.observe(ref.current);
    }

    return () => {
      observer && observer.disconnect();
    };
  }, [action, isEndOfList]);

  return (
    <div ref={ref} className="my-4">
      {isLoading && <Loader />}
      {isEndOfList && <p>{MESSAGES.END_LIST}</p>}
    </div>
  );
};
