export interface NavigationElem {
  title: string;
  url: string;
}

export interface BottomNavigationProps {
  navElements: Array<NavigationElem>;
}
