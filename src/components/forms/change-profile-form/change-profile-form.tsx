import React from 'react';
import { yupResolver } from '@hookform/resolvers/yup';
import type { SubmitHandler } from 'react-hook-form';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { useNavigate } from 'react-router-dom';

import { Button } from '../../button';
import type { ApiError } from '../../../storage/api/types';
import { useChangeProfileInfoMutation } from '../../../storage/api/profile';
import { MESSAGES } from '../../../constants';
import { Header } from '../../typography/header';

import type { InputsChangeProfileForm } from './types';
import { Field } from './field';

const schema = yup.object({
  firstName: yup.string().required(MESSAGES.REQUIRED),
  secondName: yup.string().required(MESSAGES.REQUIRED),
  about: yup.string().required(MESSAGES.REQUIRED)
});

export const ChangeProfileForm: React.FC = () => {
  const [changeProfileInfo, { error: changeInfoError, isLoading }] =
    useChangeProfileInfoMutation();
  const navigate = useNavigate();

  const {
    handleSubmit,
    control,
    formState: { errors }
  } = useForm<InputsChangeProfileForm>({
    resolver: yupResolver(schema),
    defaultValues: {
      firstName: '',
      secondName: '',
      about: ''
    }
  });

  const onSubmit: SubmitHandler<InputsChangeProfileForm> = React.useCallback(
    (values) => {
      const name = `${values.firstName} ${values.secondName}`;

      changeProfileInfo({
        name,
        about: values.about
      });

      if (!changeInfoError) {
        navigate('/profile');
      }
    },
    [changeInfoError, changeProfileInfo, navigate]
  );

  return (
    <section className="w-auto flex items-center justify-center flex-col m-auto">
      {changeInfoError && (
        <div
          id="error"
          className="my-2 bg-rose-400 p-4 w-full text-white rounded-lg"
        >
          {(changeInfoError as ApiError)?.data?.message}
        </div>
      )}
      <Header title="Мои данные" as="h1" className="self-start" />
      <form
        onSubmit={handleSubmit(onSubmit)}
        id="change-profile-form"
        className="mt-6 w-full"
      >
        <div className="columns-2">
          <Field
            control={control}
            name="firstName"
            type="text"
            placeholder="Имя"
            label="Имя"
            error={errors.firstName?.message}
          />
          <Field
            control={control}
            name="secondName"
            type="text"
            placeholder="Фамилия"
            label="Фамилия"
            error={errors.secondName?.message}
          />
        </div>
        <Field
          control={control}
          name="about"
          type="textarea"
          placeholder="О себе"
          label="О себе"
          error={errors.about?.message}
        />
        <Button
          title="Сохранить"
          type="submit"
          form="change-profile-form"
          className="mt-6 self-start"
          disabled={isLoading}
        />
      </form>
    </section>
  );
};
