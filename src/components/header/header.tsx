import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart, faUser } from '@fortawesome/free-regular-svg-icons';
import { faCartShopping } from '@fortawesome/free-solid-svg-icons';
import { Link, useLocation } from 'react-router-dom';

import { Search } from '../search';
import { Logo } from '../logo';
import { useAppSelector } from '../../hooks/use-app-selector';
import { cartSelector } from '../../storage/slices/cart/selectors';
import { productsSelector } from '../../storage/slices/products/selectors';

export const Header: React.FC = () => {
  const { pathname } = useLocation();
  const { cartProducts } = useAppSelector(cartSelector);
  const { favoritesCount = 0 } = useAppSelector(productsSelector);

  const isShowSearchField = pathname === '/products';

  return (
    <header className="bg-yellow-300 w-full py-3 sticky top-0 z-10">
      <section className="flex justify-between items-center max-w-[960px] my-0 mx-auto">
        <Link to="/" state={{ url: pathname }}>
          <Logo />
        </Link>
        {isShowSearchField && <Search />}
        <div className="flex justify-between items-center">
          <Link
            to="/favorites"
            state={{ url: pathname }}
            data-testid="favorites"
          >
            <span className="fa-layers fa-fw fa-2x">
              <FontAwesomeIcon
                icon={faHeart}
                className="cursor-pointer fill-yellow-300 w-6 h-6 fa-fw"
              />
              <span className="fa-layers-counter text-4xl font-bold">
                {favoritesCount}
              </span>
            </span>
          </Link>
          <Link
            to="/cart"
            state={{ url: pathname }}
            className="ml-8"
            data-testid="cart"
          >
            <span className="fa-layers fa-fw fa-2x">
              <FontAwesomeIcon
                icon={faCartShopping}
                className="cursor-pointer fill-yellow-300 w-6 h-6 fa-fw"
              />
              <span className="fa-layers-counter text-4xl font-bold">
                {cartProducts?.length}
              </span>
            </span>
          </Link>
          <Link
            to="/profile"
            state={{ url: pathname }}
            className="ml-8"
            data-testid="profile"
          >
            <FontAwesomeIcon
              icon={faUser}
              className="cursor-pointer fill-yellow-300 w-6 h-6 fa-fw"
            />
          </Link>
        </div>
      </section>
    </header>
  );
};
