import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/dist/query';
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER
} from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import { rootReducer } from './root-reduсer';
import { authApi } from './api/auth';
import { productsApi } from './api/products';
import { profileApi } from './api/profile';

const persistConfig = {
  key: 'root',
  storage,
  version: 1,
  // сетевые данные в localStorage не сохраняем
  blacklist: [
    authApi.reducerPath,
    productsApi.reducerPath,
    profileApi.reducerPath
  ]
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
  devTools: process.env.NODE_ENV !== 'production',
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER]
      }
    }).concat([
      authApi.middleware,
      productsApi.middleware,
      profileApi.middleware
    ])
});

setupListeners(store.dispatch);

export const persistor = persistStore(store);
