import React from 'react';
import { useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMeh } from '@fortawesome/free-regular-svg-icons';

import { Button } from '../button';

import type { ErrorProps } from './types';

export const Error: React.FC<ErrorProps> = ({ title, needButton }) => {
  const navigate = useNavigate();

  return (
    <section className="flex items-center justify-center flex-col h-full">
      <div className="text-center mt-9 text-gray-300">
        <FontAwesomeIcon icon={faMeh} className="fa-7x" />
      </div>
      <h1 className="text-4xl font-bold mt-9">{title}</h1>
      {needButton && (
        <Button
          title="На главную"
          className="mt-6"
          onClick={() => navigate('/')}
        />
      )}
    </section>
  );
};
