import React from 'react';

import type { ButtonProps } from './types';

export const Button: React.FC<ButtonProps> = ({
  children,
  title,
  type,
  className,
  icon,
  iconLeftPosition,
  iconRightPosition,
  transparent,
  fullWidth,
  disabled,
  ...rest
}) => (
  <button
    {...rest}
    disabled={disabled}
    type={type || 'button'}
    className={`${className} border rounded-full transition-colors ease-in-out duration-300 py-2 px-5 font-bold ${
      transparent
        ? 'hover:bg-yellow-300 hover:border-yellow-300'
        : 'bg-yellow-300 border-yellow-300 hover:bg-transparent hover:border-gray-200'
    } ${fullWidth && 'w-full'} ${
      disabled && 'disabled:bg-transparent disabled:border-gray-200'
    }`}
  >
    {iconLeftPosition && <span className="mr-2">{icon}</span>}
    {title}
    {iconRightPosition && <span className="ml-2">{icon}</span>}
    {children}
  </button>
);
