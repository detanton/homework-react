import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';
import { useDispatch } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';

import { Button } from '../button';
import { setUser } from '../../storage/slices/user/user';
import { setFavoritesCount } from '../../storage/slices/products/products';
import { clearCart } from '../../storage/slices/cart/cart';
import { useGetProfileQuery } from '../../storage/api/profile';
import { Error } from '../error';
import type { ApiError } from '../../storage/api/types';
import { MESSAGES } from '../../constants';

import { ProfileInfoSceleton } from './profile-info-sceleton';

export const ProfileInfo: React.FC = () => {
  const { data: user, error, isLoading } = useGetProfileQuery('');
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleClickLogout = React.useCallback(() => {
    dispatch(setUser({ accessToken: '' }));
    dispatch(setFavoritesCount(0));
    dispatch(clearCart());
    navigate('/signin');
  }, [dispatch, navigate]);

  if (isLoading) {
    return <ProfileInfoSceleton />;
  }

  if (error) {
    return <Error title={(error as ApiError)?.data?.message} />;
  }

  return (
    <section className="mt-5">
      <div className="flex">
        <div className="mr-4">
          <img
            src={user?.avatar}
            alt={user?.name}
            className="w-28 h-28 rounded-full"
          />
        </div>
        <div>
          <div className="text-xl font-extrabold">{user?.name}</div>
          <div className="mt-2 text-xs">
            <FontAwesomeIcon icon={faEnvelope} className="mr-2" />
            {user?.email}
          </div>
          <div className="text-xs mt-2">{user?.about}</div>
        </div>
      </div>
      <Link to="/profile/edit" state={{ url: '/profile' }}>
        <Button title="Изменить" transparent className="mt-10" />
      </Link>
      <Button
        title={MESSAGES.LOGOUT}
        transparent
        onClick={handleClickLogout}
        className="mt-10 block"
      />
    </section>
  );
};
