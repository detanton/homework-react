import type { ComponentType } from 'react';

export function getDisplayName<P extends object>(
  WrappedComponent: ComponentType<P>
) {
  return WrappedComponent.displayName || WrappedComponent.name || 'Component';
}
