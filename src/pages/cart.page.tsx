import React from 'react';

import { withProtection } from '../HOCs/with-protection';
import { Cart } from '../components/cart';

export const CartPage: React.FC = withProtection(() => <Cart />);
