import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';

const linkName = 'Назад';

export const TopNavigation: React.FC = () => {
  const { state } = useLocation();

  return (
    <nav className="w-[960px] mt-9">
      <Link
        to={state?.url || 'products'}
        className=" align-middle text-gray-400 hover:text-gray-600 transition-colors ease-in-out duration-300"
      >
        <FontAwesomeIcon icon={faChevronLeft} className="mr-3" />
        {linkName}
      </Link>
    </nav>
  );
};
