export const FIELDS = {
  HEADER: 'Заголовок',
  DESCRIPTION: 'Описание',
  PICTURES: 'Изображение товара',
  WEIGHT: 'Вес',
  STOCK: 'Количество, шт',
  PRICE: 'Стоимость, рубли',
  DISCOUNT: 'Скидка, %'
};

export const BUTTONS = {
  SUBMIT: 'Добавить товар'
};
