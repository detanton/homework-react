import type { ReactElement } from 'react';

export interface AlertProps {
  title: string;
  icon?: ReactElement;
  description?: Array<string>;
  className?: string;
}
