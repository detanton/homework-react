export interface PriceProps {
  discount: number | undefined;
  price: number | undefined;
  size?: 'lg' | 'xl' | '2xl' | '2xl' | '3xl' | '4xl';
}
