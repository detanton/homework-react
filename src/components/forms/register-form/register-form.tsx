import React from 'react';
import type { SubmitHandler } from 'react-hook-form';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from 'yup';
import { Link, useLocation, useNavigate } from 'react-router-dom';

import { Button } from '../../button';
import { useSignUpMutation } from '../../../storage/api/auth';
import type { ApiError } from '../../../storage/api/types';
import { MESSAGES } from '../../../constants';

import { Field } from './field';
import type { Inputs } from './types';

const schema = yup.object({
  email: yup
    .string()
    .email('Не соответствует формату email')
    .required('Обязательное поле'),
  password: yup
    .string()
    .min(6, 'Должно быть минимум 6 символов')
    .max(24)
    .required('Обязательное поле'),
  group: yup
    .string()
    .lowercase()
    .matches(/ra-/, 'Не соответствует формату')
    .required('Обязательное поле')
});

export const RegisterForm: React.FC = () => {
  const [signUp, { error: errorSignUp, isLoading, isError }] =
    useSignUpMutation();
  const navigate = useNavigate();
  const { pathname } = useLocation();

  const {
    handleSubmit,
    control,
    formState: { errors }
  } = useForm<Inputs>({
    resolver: yupResolver(schema),
    defaultValues: {
      email: '',
      group: '',
      password: ''
    }
  });

  const onSubmit: SubmitHandler<Inputs> = React.useCallback(
    (values) => {
      signUp(values);

      if (!isError) {
        navigate('/signin');
      }
    },
    [isError, navigate, signUp]
  );

  return (
    <section className="w-96 flex items-center justify-center flex-col h-full">
      {errorSignUp && (
        <div className="my-2 bg-rose-400 p-4 w-full text-white rounded-lg">
          {(errorSignUp as ApiError)?.data?.message}
        </div>
      )}
      <h1 className="text-2xl font-extrabold self-start">Регистрация</h1>
      <form onSubmit={handleSubmit(onSubmit)} className="mt-6 w-full">
        <Field
          control={control}
          name="email"
          type="text"
          placeholder="Email"
          label="Email"
          error={errors.email?.message}
        />
        <Field
          control={control}
          name="password"
          type="password"
          placeholder="Пароль"
          label="Пароль"
          maxLength={24}
          error={errors.password?.message}
        />
        <Field
          control={control}
          name="group"
          type="text"
          placeholder="ra-<номер_вашего_потока>" //ra-2
          label="Группа"
          error={errors.group?.message}
        />
        <p className="mt-6 text-sm">{MESSAGES.SIGNUP_AGREEMENT}</p>
        <Button
          title="Зарегистрироваться"
          type="submit"
          fullWidth
          className="mt-6"
          disabled={isLoading}
        />
      </form>
      <Link
        to="/signin"
        state={{ url: pathname }}
        className="mt-6 font-bold transition-colors ease-in-out duration-00 hover:text-yellow-500"
      >
        {MESSAGES.SIGNIN}
      </Link>
    </section>
  );
};
