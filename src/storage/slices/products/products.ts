import { createSlice } from '@reduxjs/toolkit';

import type { InitialStateProducts } from './types';

const initialState: InitialStateProducts = {
  favoritesCount: 0
};

export const productsSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    setFavoritesCount: (state, action) => {
      state.favoritesCount = action.payload;
    },
    toggleIsFavorite: (state, action) => {
      state.favoritesCount = action.payload
        ? state.favoritesCount - 1
        : state.favoritesCount + 1;
    }
  }
});

export const sliceName = 'products';

export const { setFavoritesCount, toggleIsFavorite } = productsSlice.actions;

export default productsSlice.reducer;
