import React from 'react';

import { Button } from '../button';
import { useAppSelector } from '../../hooks/use-app-selector';
import { cartSelectorSummary } from '../../storage/slices/cart/selectors';

export const CartSummary: React.FC = () => {
  const { total, productsPrice, productsDiscount, summary } =
    useAppSelector(cartSelectorSummary);

  return (
    <div className="w-[284px] shadow-xl rounded-xl p-4">
      <h3 className="text-xl font-extrabold">Ваша корзина</h3>
      <p className="mt-6 flex justify-between text-sm">
        <span className="text-gray-500">{`Товары (${total})`}</span>
        <span className="font-semibold">{`${(
          Math.round(productsPrice * 10) / 10
        ).toFixed(1)} ₽`}</span>
      </p>
      <p className="mt-4 pb-4 border-b flex justify-between text-sm">
        <span className="text-gray-500">Скидка</span>
        <span className="text-red-500 font-semibold">{`- ${(
          Math.round(productsDiscount * 10) / 10
        ).toFixed(1)} ₽`}</span>
      </p>
      <p className="mt-4 text-sm flex justify-between">
        <span className="font-extrabold">Общая стоимость</span>
        <span className="font-extrabold text-xl">{`${(
          Math.round(summary * 10) / 10
        ).toFixed(1)} ₽`}</span>
      </p>
      <Button title="Оформить заказ" fullWidth className="mt-6" />
    </div>
  );
};
