import { fetchBaseQuery } from '@reduxjs/toolkit/query';

import type { RootState } from '../storage/types';
import { baseApiUrl } from '../constants';

export const customBaseQuery = fetchBaseQuery({
  baseUrl: baseApiUrl,
  prepareHeaders: (headers, { getState }) => {
    const accessToken = (getState() as RootState).user.accessToken;
    headers.set('Content-Type', 'application/json');

    if (accessToken) {
      headers.set('authorization', `Bearer ${accessToken}`);
    }

    return headers;
  }
});
