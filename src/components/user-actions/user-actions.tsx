import React from 'react';
import { Link } from 'react-router-dom';

export const UserActions = () => {
  return (
    <aside className="p-3 bg-slate-50 rounded-xl">
      <h3 className="text-lg font-extrabold border-b pb-4">
        Доступные действия
      </h3>
      <Link
        to="/products/create"
        className="inline-block mt-6 transition-colors ease-in-out duration-300 hover:text-yellow-500"
      >
        Создать продукт
      </Link>
    </aside>
  );
};
