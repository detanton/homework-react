import React from 'react';

import type { DiscountProps } from './types';

export const Discount: React.FC<DiscountProps> = ({ discount }) =>
  Boolean(discount) && (
    <span className="absolute top-0 bg-red-500 rounded-full py-[2px] px-2 text-white font-extrabold">
      {`-${discount} %`}
    </span>
  );
