import React from 'react';

import type { HeaderProps } from './types';

export const Header: React.FC<HeaderProps> = ({
  title,
  className = '',
  as = 'h1'
}) =>
  React.createElement(
    as,
    { className: `text-3xl font-extrabold ${className}` },
    title
  );
