import React from 'react';
import { useSearchParams } from 'react-router-dom';

import { Error } from '../error';
import { Card } from '../card';
import { useGetProductsQuery } from '../../storage/api/products';
import type { ApiError } from '../../storage/api/types';
import { InfiniteScroll } from '../infinite-scroll';
import { SEARCH_PARAMS_KEY } from '../../constants';
import { Loader } from '../loader';

export const CardList: React.FC = () => {
  const [searchParams] = useSearchParams();
  const [localSearch, setLocalSearch] = React.useState({
    page: 1
  });
  const query = searchParams.get(SEARCH_PARAMS_KEY) ?? '';
  const { data, error, isLoading, isFetching } = useGetProductsQuery({
    query,
    page: query ? 1 : localSearch.page
  });

  const isEndOfList = React.useMemo(() => {
    return !query && data && data.products.length >= data.total;
  }, [data, query]);

  // Если есть посковая фраза, то сбрасываем page
  React.useEffect(() => {
    setLocalSearch({ page: query ? 1 : localSearch.page });
  }, [localSearch.page, query]);

  const loadMorePosts = React.useCallback(() => {
    if (!isEndOfList) {
      setLocalSearch((prevState) => ({
        ...prevState,
        page: prevState.page + 1
      }));
    }
  }, [isEndOfList]);

  if (error) {
    return <Error title={(error as ApiError)?.data?.message} />;
  }

  if (isLoading) {
    return <Loader />;
  }

  if (data?.products.length === 0) {
    return <Error title="Товары не найдены" />;
  }

  return (
    <section id="card-list" className="h-full">
      <div className="grid lg:grid-cols-4 md:grid-cols-3 grid-cols-2 gap-x-4 gap-y-10 my-5 lg:my-10 mx-4 lg:mx-0">
        {data?.products.map((product) => (
          <Card key={product._id} {...product} />
        ))}
      </div>
      {data && (
        <InfiniteScroll
          isLoading={isFetching}
          action={loadMorePosts}
          isEndOfList={isEndOfList}
        />
      )}
    </section>
  );
};
