import type { Profile } from '../../../components/profile-info/types';

export interface InitialStateUser {
  data: Profile | null;
  error: string | undefined;
  isLoading: boolean;
  accessToken: string;
}
