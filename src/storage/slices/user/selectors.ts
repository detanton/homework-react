import { createSelector } from '@reduxjs/toolkit';

import type { RootState } from '../../types';

import { sliceName } from './user';

export const userSelector = createSelector(
  (state: RootState) => state[sliceName].data,
  (state: RootState) => state[sliceName].error,
  (state: RootState) => state[sliceName].isLoading,
  (data, error, isLoading) => ({ data, error, isLoading })
);

export const accessTokenSelector = createSelector(
  (state: RootState) => state[sliceName].accessToken,
  (accessToken) => accessToken
);
