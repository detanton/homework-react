import React from 'react';
import { useParams, Link, useLocation, useNavigate } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faAward,
  faStar,
  faTruck,
  faHeart
} from '@fortawesome/free-solid-svg-icons';
import {
  faHeart as faHeartSolid,
  faTrashCan
} from '@fortawesome/free-regular-svg-icons';
import { useDispatch } from 'react-redux';

import { Discount } from '../discount';
import { Price } from '../price/price';
import { Counter } from '../counter';
import { Button } from '../button';
import { Alert } from '../alert';
import {
  useGetProductQuery,
  useRemoveProductMutation,
  useToggleFavoriteMutation
} from '../../storage/api/products';
import type { ServerError } from '../../storage/api/types';
import { Error } from '../error';
import {
  addToCart,
  changeCountProductToCart
} from '../../storage/slices/cart/cart';
import { useAppSelector } from '../../hooks/use-app-selector';
import {
  cartSelector,
  cartSelectorProduct
} from '../../storage/slices/cart/selectors';
import { userSelector } from '../../storage/slices/user/selectors';
import { toggleIsFavorite } from '../../storage/slices/products/products';
import { Loader } from '../loader';

export const SingleProduct: React.FC = () => {
  const { pathname } = useLocation();
  const { id = '' } = useParams();
  const navigate = useNavigate();
  const { data, error, isLoading } = useGetProductQuery({ id });
  const { data: user } = useAppSelector(userSelector);
  const [toggleFavorite] = useToggleFavoriteMutation();
  const [removeProduct] = useRemoveProductMutation();
  const dispatch = useDispatch();
  const [localCount, setLocalCount] = React.useState(1);
  const [isFavorite, setFavorite] = React.useState(false);
  const { cartProducts } = useAppSelector(cartSelector);
  const { cartProduct } = useAppSelector((state) =>
    cartSelectorProduct(state, id)
  );

  const count = cartProduct?.count || localCount;
  const name = data?.name;
  const description = data?.description || '';
  const price = data?.price || 1;
  const discount = data?.discount || 0;
  const pictures = data?.pictures;
  const wight = data?.wight;
  const stock = data?.stock;
  const reviewsCount = data?.reviews.length;
  const disabledButton =
    cartProducts.findIndex((product) => product.id === data?._id) !== -1;
  const isUserCreateProduct = data?.author._id === user?._id;

  const handleClickAddToCart = React.useCallback(() => {
    dispatch(
      addToCart({ id, count, price, discount, pictures, wight, name, stock })
    );
  }, [count, discount, dispatch, id, name, pictures, price, stock, wight]);

  const handleClickMinus = React.useCallback(
    (value) => {
      setLocalCount(value);
      dispatch(
        changeCountProductToCart({
          id,
          count: value
        })
      );
    },
    [dispatch, id]
  );

  const handleClickPlus = React.useCallback(
    (value) => {
      setLocalCount(value);
      dispatch(
        changeCountProductToCart({
          id,
          count: value
        })
      );
    },
    [dispatch, id]
  );

  const handleCkickToggleFavorite = React.useCallback(() => {
    toggleFavorite({ id, toggle: isFavorite });
    setFavorite(!isFavorite);
    dispatch(toggleIsFavorite(isFavorite));
  }, [dispatch, id, isFavorite, toggleFavorite]);

  const handleRemoveProduct = React.useCallback(() => {
    removeProduct({ id: data?._id || '' });
    navigate('/products');
  }, [data?._id, navigate, removeProduct]);

  React.useEffect(() => {
    const isFavorite = data?.likes.includes(user?._id || '') || false;
    setFavorite(isFavorite);
  }, [data?.likes, user?._id]);

  if (isLoading) {
    return <Loader />;
  }

  if (error) {
    return <Error title={(error as ServerError).message} />;
  }

  return (
    <section className="my-4">
      <h1 className="text-3xl font-extrabold">{name}</h1>
      <div className="flex justify-between">
        <section className="mt-1">
          <div>
            <span className="text-gray-400 mr-4">
              Артикул: <span className="text-black">2388907</span>
            </span>
            <span className="text-yellow-500 mr-4">
              {[...Array(5).keys()].map((item) => (
                <FontAwesomeIcon key={item} icon={faStar} className="mr-1" />
              ))}
            </span>
            <span className="text-yellow-500">
              <Link
                to="/reviews"
                state={{ url: pathname }}
              >{`${reviewsCount} отзывов`}</Link>
            </span>
          </div>
          <figure className="mt-5 relative rounded-md">
            <Discount discount={discount} />
            <img
              src={pictures}
              alt={name}
              loading="lazy"
              className="w-[488px] h-[488px] object-contain"
            />
          </figure>
        </section>
        <section className="w-[343px] mt-9">
          <Price price={price * count} discount={data?.discount} size="3xl" />
          <div className="flex mt-6">
            <Counter
              initialValue={count}
              max={stock || 1}
              onClickMinus={handleClickMinus}
              onClickPlus={handleClickPlus}
            />
            <Button
              title="В корзину"
              onClick={handleClickAddToCart}
              fullWidth
              disabled={disabledButton}
              className="ml-4"
            />
          </div>
          <div className="mt-6 text-gray-400">
            <button onClick={handleCkickToggleFavorite}>
              <FontAwesomeIcon
                icon={isFavorite ? faHeart : faHeartSolid}
                className={`text-xl ${isFavorite && 'text-red-500'}`}
              />
              <span className="ml-2">В избранное</span>
            </button>
            {isUserCreateProduct && (
              <button
                title="remove"
                data-testid="remove"
                className="hover:cursor-pointer ml-6"
                onClick={handleRemoveProduct}
              >
                <FontAwesomeIcon
                  icon={faTrashCan}
                  className="text-red-500 mr-2"
                />
                Удалить
              </button>
            )}
          </div>
          <div className="mt-6">
            <Alert
              title="Доставка по всему Миру!"
              icon={<FontAwesomeIcon icon={faTruck} className="fa-fw" />}
              description={[
                'Доставка курьером — от 399 ₽',
                'Доставка в пункт выдачи — от 199 ₽'
              ]}
            />
          </div>
          <div className="mt-3">
            <Alert
              title="Гарантия качества"
              icon={<FontAwesomeIcon icon={faAward} className="fa-fw" />}
              description={[
                'Если Вам не понравилось качество нашей продукции, мы вернем деньги, либо сделаем все возможное, чтобы удовлетворить ваши нужды.'
              ]}
            />
          </div>
        </section>
      </div>
      {data?.description && (
        <section className="mt-10">
          <h2 className="font-extrabold text-2xl">Описание</h2>
          <div dangerouslySetInnerHTML={{ __html: description }} />
        </section>
      )}
    </section>
  );
};
