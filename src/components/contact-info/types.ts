export enum socialMediaTypes {
  telegram = 'telegram',
  viber = 'viber',
  whatapp = 'whatapp',
  instagram = 'instagram',
  vk = 'vk'
}

export interface SocialMedia {
  type: socialMediaTypes | string;
  url: string;
}

export interface ContactInfoProps {
  title?: string;
  phone?: string;
  email?: string;
  socialMedia?: Array<SocialMedia>;
}
