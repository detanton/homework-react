import type { Config } from 'jest';

const config: Config = {
  preset: 'ts-jest/presets/js-with-ts',
  clearMocks: true,
  automock: false,
  rootDir: '..',
  resetMocks: false,
  testEnvironment: 'jsdom',
  moduleDirectories: ['node_modules', 'src'],
  moduleNameMapper: {
    '\\.(css|less|scss|sass)$': 'identity-obj-proxy',
    '@root(.*)$': '<rootDir>/src/$1'
  },
  transform: {
    '\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$':
      '<rootDir>/jest/file-transformer.ts'
  },
  setupFiles: ['<rootDir>/jest/setup-jest.ts'],
  setupFilesAfterEnv: ['<rootDir>/jest/setup-jest.ts']
};

export default config;
