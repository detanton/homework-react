import type { SyntheticEvent } from 'react';
import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faMagnifyingGlass,
  faCircleXmark
} from '@fortawesome/free-solid-svg-icons';
import { useSearchParams } from 'react-router-dom';

import { useDebounce } from '../../hooks/use-debounce';
import { SEARCH_PARAMS_KEY } from '../../constants';

export const Search: React.FC = () => {
  const [searchParams, setSearchParams] = useSearchParams();
  const [localSearchPhrase, setLocalSearchPhrase] = React.useState(
    searchParams.get(SEARCH_PARAMS_KEY) || ''
  );

  const optimizedValue = useDebounce({
    value: localSearchPhrase,
    delay: 1000
  });

  const handleChangeValue = React.useCallback(
    (e: SyntheticEvent<HTMLInputElement>) => {
      const value = e.currentTarget.value;

      setLocalSearchPhrase(value);
    },
    []
  );

  const handleClickReset = React.useCallback(() => {
    setLocalSearchPhrase('');
  }, []);

  React.useEffect(() => {
    optimizedValue
      ? searchParams.set(SEARCH_PARAMS_KEY, optimizedValue)
      : searchParams.delete(SEARCH_PARAMS_KEY);

    setSearchParams(searchParams);
  }, [searchParams, setSearchParams, optimizedValue, localSearchPhrase]);

  return (
    <div className="relative">
      <label htmlFor="search">
        <input
          id="search"
          type="text"
          aria-label="search"
          placeholder="Найти товар"
          value={localSearchPhrase}
          className="p-3 rounded-3xl w-[428px] transition-all ease-in-out duration-300 outline-none border border-yellow-300 focus:ring-2 focus:ring-offset-2 focus:ring-yellow-500 "
          onChange={handleChangeValue}
        />
        <button
          type="button"
          title="search"
          className="absolute top-3 right-5 cursor-pointer"
          onClick={handleClickReset}
        >
          <FontAwesomeIcon
            icon={localSearchPhrase ? faCircleXmark : faMagnifyingGlass}
            className="text-gray-500 fa-fw align-middle"
          />
        </button>
      </label>
    </div>
  );
};
