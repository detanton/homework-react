import { createSelector } from '@reduxjs/toolkit';

import type { RootState } from '../../types';

import { sliceName } from './cart';

export const cartSelector = createSelector(
  (state: RootState) => state[sliceName].cartProducts,
  (cartProducts) => ({ cartProducts })
);

export const cartSelectorProduct = createSelector(
  (state: RootState, id: string) =>
    state[sliceName].cartProducts.find((product) => product.id === id),
  (cartProduct) => ({ cartProduct })
);

export const cartSelectorSummary = createSelector(
  (state: RootState) => state[sliceName].total,
  (state: RootState) =>
    state[sliceName].cartProducts.reduce(
      (summ, product) => summ + product.price * product.count,
      0
    ),
  (state: RootState) =>
    state[sliceName].cartProducts.reduce(
      (summ, product) =>
        summ + (product.price / 100) * product.discount * product.count,
      0
    ),
  (state: RootState) =>
    state[sliceName].cartProducts.reduce(
      (summ, product) =>
        summ +
        (product.price - (product.price / 100) * product.discount) *
          product.count,
      0
    ),
  (total, productsPrice, productsDiscount, summary) => ({
    total,
    productsPrice,
    productsDiscount,
    summary
  })
);
