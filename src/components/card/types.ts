export interface Author {
  _id: string;
  about: string;
  avatar: string;
  email: string;
  name: string;
}

export interface Review {
  _id: string;
  author: Author;
  created_at: string;
  updated_at: string;
  product: string;
  rating: number;
  text: string;
}

export interface Product {
  _id: string;
  name: string;
  description: string;
  pictures: string;
  price: number;
  discount: number;
  wight: string;
  isCart: boolean;
  available: boolean;
  stock: number;
  created_at: string;
  updated_at: string;
  isPublished: boolean;
  tags: Array<string>;
  reviews: Array<Review>;
  likes: Array<string>;
  author: Author;
}

export interface CardProps extends Product {}
