import React from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashCan } from '@fortawesome/free-regular-svg-icons';
import { useDispatch } from 'react-redux';

import type { Product } from '../card/types';
import { Counter } from '../counter';
import { Price } from '../price/price';
import {
  changeCountProductToCart,
  removeFromCart
} from '../../storage/slices/cart/cart';
import { useAppSelector } from '../../hooks/use-app-selector';
import { cartSelectorProduct } from '../../storage/slices/cart/selectors';

export const CartProduct: React.FC<Partial<Product>> = ({
  _id = '',
  name,
  wight,
  pictures,
  price = 0,
  discount = 0,
  stock = 0
}) => {
  const dispatch = useDispatch();
  const { cartProduct } = useAppSelector((state) =>
    cartSelectorProduct(state, _id)
  );

  const count = cartProduct?.count || 1;

  const handleClickRemove = React.useCallback(() => {
    dispatch(removeFromCart({ id: _id }));
  }, [_id, dispatch]);

  const handlClickMinus = React.useCallback(
    (value) => {
      dispatch(changeCountProductToCart({ id: _id, count: value }));
    },
    [_id, dispatch]
  );

  const handlClickPlus = React.useCallback(
    (value) => {
      dispatch(changeCountProductToCart({ id: _id, count: value }));
    },
    [_id, dispatch]
  );

  return (
    <div className="flex py-6 items-center justify-between border-b last:border-none">
      <div className="w-16 h-16 mr-4">
        <img src={pictures} alt="" loading="lazy" className="w-16 h-16" />
      </div>
      <div className="mr-8 w-[224px]">
        <Link to={`/products/${_id}`}>
          <h3 className="text-sm font-bold hover:text-yellow-400 transition-colors ease-in-out duration-300">
            {name}
          </h3>
        </Link>
        <p className="mt-2 text-xs text-gray-400">{wight}</p>
      </div>
      <div>
        <Counter
          initialValue={count}
          max={stock}
          onClickMinus={(value) => handlClickMinus(value)}
          onClickPlus={(value) => handlClickPlus(value)}
        />
      </div>
      <div className="ml-8 w-24 text-right">
        <Price price={price * count} discount={discount} size="xl" />
      </div>
      <div className="ml-8">
        <button onClick={handleClickRemove}>
          <FontAwesomeIcon icon={faTrashCan} className="fa-fw" />
        </button>
      </div>
    </div>
  );
};
