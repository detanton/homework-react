import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import type { ContactInfoProps } from './types';
import { getSocialIcon } from './utils';

export const ContactInfo: React.FC<ContactInfoProps> = ({
  title = '',
  phone = '',
  email = '',
  socialMedia = []
}) => (
  <section className="flex flex-col justify-between">
    {title && <h3 className="font-bold">{title}</h3>}
    <div className="mt-4">
      {phone && <h4 className="font-bold">{phone}</h4>}
      {email && <h5 className="text-xs">{email}</h5>}
    </div>
    {socialMedia.length !== 0 && (
      <ul className="flex justify-between w-40 mt-4">
        {socialMedia.map(({ type, url }) => (
          <li key={type}>
            <a href={url} target="_blank" rel="noopener noreferrer">
              <FontAwesomeIcon icon={getSocialIcon(type)} className="w-6 h-6" />
            </a>
          </li>
        ))}
      </ul>
    )}
  </section>
);
