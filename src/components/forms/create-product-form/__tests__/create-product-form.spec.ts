import '@testing-library/jest-dom';
import { describe, expect, it } from '@jest/globals';
import { fireEvent, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { TestWrapperProvider } from '../../../../utils/test-wrapper';
import { BUTTONS } from '../constants';
import { MESSAGES } from '../../../../constants';

describe('Тестирование формы создания продукта', () => {
  let submitButton: HTMLElement;
  let textFields: Array<HTMLElement>;
  let numberFields: Array<HTMLElement>;
  let formFields: Array<HTMLElement>;

  beforeEach(() => {
    TestWrapperProvider({
      initialRouterEntires: ['/products/create']
    });

    submitButton = screen.getByRole('button', { name: BUTTONS.SUBMIT });
    textFields = screen.getAllByRole('textbox');
    numberFields = screen.getAllByRole('spinbutton');
    formFields = [...textFields, ...numberFields];
  });

  describe('Тестирование формы', () => {
    it('Рендер полей', () => {
      formFields.forEach((field) => {
        expect(field).toBeDefined();
      });
    });

    it('Кнопка Добавить товар доступна', () => {
      expect(submitButton).toBeTruthy();
    });

    describe('Отправка формы', () => {
      beforeEach(async () => {
        await userEvent.click(submitButton);
      });

      describe('Неверные значения полей', () => {
        describe('Пустые поля', () => {
          it('Заполение полей', () => {
            const fieldValues = ['', '', '', '100 г', 15, 5600, 15];
            formFields.forEach((field: HTMLInputElement, index) => {
              fireEvent.change(field, {
                target: { value: fieldValues[index] }
              });

              expect(field.value).toEqual(String(fieldValues[index]));
            });
          });

          it('Наличие ошибок', async () => {
            expect(await screen.findAllByText(MESSAGES.REQUIRED)).toHaveLength(
              3
            );
          });
        });
      });
    });

    describe('Верные значения полей', () => {
      describe('Поля заполнены правильно', () => {
        it('Заполение полей', async () => {
          const fieldValues = [
            'New Product',
            'Description product',
            'https://c.dns-shop.ru/thumb/st4/fit/wm/0/0/abcf83f0449c02a97ca189c1180191ee/5582e87ee92b0104f8803513b0d40afa7dc5140240e0ef2b26a770e512d26dd4.jpg.webp',
            '100 г',
            15,
            5600,
            15
          ];
          formFields.forEach((field: HTMLInputElement, index) => {
            fireEvent.change(field, { target: { value: fieldValues[index] } });
            expect(field.value).toEqual(String(fieldValues[index]));
          });

          await userEvent.click(submitButton);

          expect(screen.queryAllByText(MESSAGES.REQUIRED)).toEqual([]);
          expect(screen.queryByTestId('api-error'));
        });
      });
    });
  });
});
