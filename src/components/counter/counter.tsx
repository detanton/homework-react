import React from 'react';
import { faMinus, faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import type { CounterProps } from './types';

export const Counter: React.FC<CounterProps> = ({
  initialValue,
  max,
  onClickMinus,
  onClickPlus
}) => {
  const [count, setCount] = React.useState(initialValue);

  const handlClickMinus = React.useCallback(() => {
    const value = count !== 1 ? count - 1 : count;
    setCount(value);
    onClickMinus && onClickMinus(value);
  }, [count, onClickMinus]);

  const handlClickPlus = React.useCallback(() => {
    const value = count === max ? count : count + 1;
    setCount(value);
    onClickPlus && onClickPlus(value);
  }, [count, max, onClickPlus]);

  return (
    <div className="border rounded-full h-12 flex items-center justify-around p-3">
      <button onClick={handlClickMinus} disabled={count === 1}>
        <FontAwesomeIcon
          icon={faMinus}
          className={`${count === 1 && 'text-gray-300'} 'fa-fw'`}
        />
      </button>
      <div className="font-bold text-xl mx-2 w-6 text-center">{count}</div>
      <button
        onClick={handlClickPlus}
        disabled={count === max}
        className={`${count === max && 'text-gray-300'} 'fa-fw'`}
      >
        <FontAwesomeIcon icon={faPlus} className="fa-fw" />
      </button>
    </div>
  );
};
