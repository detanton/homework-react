import { combineReducers } from '@reduxjs/toolkit';

import userReducer from './slices/user/user';
import productsReducer from './slices/products/products';
import cartSlice from './slices/cart/cart';
import { authApi } from './api/auth';
import { productsApi } from './api/products';
import { profileApi } from './api/profile';

export const rootReducer = combineReducers({
  user: userReducer,
  products: productsReducer,
  cart: cartSlice,
  [authApi.reducerPath]: authApi.reducer,
  [productsApi.reducerPath]: productsApi.reducer,
  [profileApi.reducerPath]: profileApi.reducer
});
