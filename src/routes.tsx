import React from 'react';
import type { RouteObject } from 'react-router-dom';
import { createBrowserRouter } from 'react-router-dom';

import { CatalogPage } from './pages/catalog.page';
import { HomePage } from './pages/home.page';
import { ProductPage } from './pages/product.page';
import { NotFoundPage } from './pages/not-found.page';
import { ProfilePage } from './pages/profile.page';
import { FavoritesPage } from './pages/favorites.page';
import { RootLayout } from './layouts/root-layout';
import { SignUpPage } from './pages/sign-up.page';
import { SignInPage } from './pages/sign-in.page';
import { CartPage } from './pages/cart.page';
import { CreateProductPage } from './pages/create-product.page';
import { ProfileEditPage } from './pages/profile-edit.page';

export const routerConfig: Array<RouteObject> = [
  {
    path: '/',
    element: <RootLayout />,
    children: [
      {
        index: true,
        element: <HomePage />
      },
      {
        path: 'signup',
        element: <SignUpPage />
      },
      {
        path: 'signin',
        element: <SignInPage />
      },
      {
        path: 'products',
        element: <CatalogPage />
      },
      {
        path: 'products/:id',
        element: <ProductPage />
      },
      {
        path: 'products/create',
        element: <CreateProductPage />
      },
      {
        path: 'favorites',
        element: <FavoritesPage />
      },
      {
        path: 'profile',
        element: <ProfilePage />
      },
      {
        path: 'profile/edit',
        element: <ProfileEditPage />
      },
      {
        path: 'cart',
        element: <CartPage />
      },
      {
        path: '*',
        element: <NotFoundPage />
      }
    ]
  }
];

export const router = createBrowserRouter(routerConfig, {
  basename: process.env.PUBLIC_PATH || '/'
});
