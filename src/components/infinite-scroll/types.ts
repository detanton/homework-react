export interface InfiniteScrollProps {
  action: () => void;
  isLoading?: boolean;
  isEndOfList?: boolean;
}
