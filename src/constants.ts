export const groupId = 'ra-2';
export const baseApiUrl = `https://api.react-learning.ru/v2/${groupId}`;
export const SEARCH_PARAMS_KEY = 'query';
export const MESSAGES = {
  REQUIRED: 'Поле обязательное для заполениния',
  INVALID_EMAIL: 'Не соответствует формату email',
  END_LIST: 'Конец списка',
  SIGNIN: 'Войти',
  SIGNUP: 'Зарегистрироваться',
  SIGNUP_AGREEMENT:
    'Регистрируясь на сайте, вы соглашаетесь с нашими Правилами и Политикой конфиденциальности и соглашаетесь на информационную рассылку.',
  LOGOUT: 'Выйти'
};
