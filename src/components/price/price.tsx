import React from 'react';

import type { PriceProps } from './types';

export const Price: React.FC<PriceProps> = ({
  price = 0,
  discount = 0,
  size
}) => {
  const isHaveDiscount = Boolean(discount);

  return (
    <p
      className={`relative text-${size} font-extrabold w-full ${
        isHaveDiscount && 'text-red-500'
      }`}
    >
      {isHaveDiscount && (
        <span className="absolute -top-3 text-xs line-through font-semibold text-black">{`${price} ₽`}</span>
      )}
      {isHaveDiscount
        ? `${(Math.round((price - (price / 100) * discount) * 10) / 10).toFixed(
            1
          )} ₽`
        : `${price} ₽`}
    </p>
  );
};
