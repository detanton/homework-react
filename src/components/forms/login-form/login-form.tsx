import React from 'react';
import type { SubmitHandler } from 'react-hook-form';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Link, useLocation, useNavigate } from 'react-router-dom';
import * as yup from 'yup';

import { Button } from '../../button';
import { useSignInMutation } from '../../../storage/api/auth';
import type { ApiError } from '../../../storage/api/types';
import { MESSAGES } from '../../../constants';

import { Field } from './field';
import type { InputsLoginForm } from './types';

const schema = yup.object({
  email: yup.string().email(MESSAGES.INVALID_EMAIL).required(MESSAGES.REQUIRED),
  password: yup
    .string()
    .min(6, 'Должно быть минимум 6 символов')
    .max(24)
    .required(MESSAGES.REQUIRED)
});

export const LoginForm: React.FC = () => {
  const [signIn, { error: errorSignIn, isLoading, data: user }] =
    useSignInMutation();
  const navigate = useNavigate();
  const { pathname } = useLocation();

  const {
    handleSubmit,
    control,
    formState: { errors }
  } = useForm<InputsLoginForm>({
    resolver: yupResolver(schema),
    defaultValues: {
      email: '',
      password: ''
    }
  });

  const onSubmit: SubmitHandler<InputsLoginForm> = React.useCallback(
    (values) => {
      signIn(values);
    },
    [signIn]
  );

  React.useEffect(() => {
    if (user?.data) {
      navigate('/products');
    }
  }, [navigate, user]);

  return (
    <section className="w-96 flex items-center justify-center flex-col h-full">
      {errorSignIn && (
        <div
          id="error"
          className="my-2 bg-rose-400 p-4 w-full text-white rounded-lg"
        >
          {(errorSignIn as ApiError)?.data?.message}
        </div>
      )}
      <h1 className="text-2xl font-extrabold self-start">Вход</h1>
      <form onSubmit={handleSubmit(onSubmit)} className="mt-6 w-full">
        <Field
          control={control}
          name="email"
          type="text"
          placeholder="Email"
          label="Email"
          error={errors.email?.message}
        />
        <Field
          control={control}
          name="password"
          type="password"
          placeholder="Пароль"
          label="Пароль"
          maxLength={24}
          error={errors.password?.message}
        />
        <Button
          title={MESSAGES.SIGNIN}
          type="submit"
          fullWidth
          className="mt-6"
          disabled={isLoading}
        />
      </form>
      <Link
        to="/signup"
        state={{ url: pathname }}
        className="mt-6 font-bold transition-colors ease-in-out duration-00 hover:text-yellow-500"
      >
        {MESSAGES.SIGNUP}
      </Link>
    </section>
  );
};
