import React from 'react';

import { Card } from '../card';
import { Error } from '../error';
import { useGetFavoriteProductsQuery } from '../../storage/api/products';
import { userSelector } from '../../storage/slices/user/selectors';
import { useAppSelector } from '../../hooks/use-app-selector';
import { Loader } from '../loader';

export const FavoritesList: React.FC = () => {
  const { data: user } = useAppSelector(userSelector);
  const { data, isFetching } = useGetFavoriteProductsQuery(user?._id || '');

  const favoriteProductsLength = React.useMemo(() => {
    return data?.products?.length === 0;
  }, [data?.products?.length]);

  if (favoriteProductsLength) {
    return <Error title="Нет избранных товаров" />;
  }

  if (isFetching) {
    return <Loader />;
  }

  return (
    <section>
      <div className="grid lg:grid-cols-4 md:grid-cols-3 grid-cols-2 gap-x-4 gap-y-10 my-5 lg:my-10 mx-4 lg:mx-0">
        {data?.products?.map((product) => (
          <Card key={product._id} {...product} />
        ))}
      </div>
    </section>
  );
};
