import { useEffect, useRef, useState } from 'react';

interface useThrottle<ValueType> {
  value: ValueType;
  delay: number;
}

export const useThrottle = <ValueType = unknown>({
  value,
  delay
}: useThrottle<ValueType>) => {
  const [throttledValue, setThrottledValue] = useState(value);

  const ref = useRef({
    value,
    inProgress: false
  });

  useEffect(() => {
    ref.current.value = value;

    if (ref.current.inProgress) {
      return;
    }

    ref.current.inProgress = true;

    setInterval(() => {
      setThrottledValue(ref.current.value);
      ref.current.inProgress = false;
    }, delay);
  }, [delay, value]);

  return throttledValue;
};
