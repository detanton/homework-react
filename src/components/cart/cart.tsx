import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTruck } from '@fortawesome/free-solid-svg-icons';

import { useAppSelector } from '../../hooks/use-app-selector';
import {
  cartSelector,
  cartSelectorSummary
} from '../../storage/slices/cart/selectors';
import { Alert } from '../alert';

import { CartProduct } from './cart-product';
import { CartSummary } from './cart-summary';

export const Cart = () => {
  const { cartProducts: cart } = useAppSelector(cartSelector);
  const { total } = useAppSelector(cartSelectorSummary);

  return (
    <section>
      <h1 className="text-3xl text-left mt-6">
        В корзине товаров
        <span className="font-extrabold"> {total}</span>
      </h1>
      <div className="flex justify-between">
        <div className="mr-16">
          {cart?.map((product) => (
            <CartProduct key={product.id} {...product} _id={product.id} />
          ))}
        </div>
        {total !== 0 && (
          <div className="flex flex-col">
            <CartSummary />
            <div className="mt-8">
              <Alert
                title="Доставка по всему Миру!"
                icon={<FontAwesomeIcon icon={faTruck} className="fa-fw" />}
                description={[
                  'Доставка курьером — от 399 ₽',
                  'Доставка в пункт выдачи — от 199 ₽'
                ]}
              />
            </div>
          </div>
        )}
      </div>
    </section>
  );
};
