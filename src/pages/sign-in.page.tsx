import React from 'react';

import { LoginForm } from '../components/forms/login-form';

export const SignInPage: React.FC = () => <LoginForm />;
