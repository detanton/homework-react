import { cypressSignIn } from './utils';

describe('Создание поста', () => {
  beforeEach(() => {
    cypressSignIn();
    cy.visit('/profile');
  });

  it('Создаем продукт', () => {
    cy.wait(1000);
    cy.get('a[href="/products/create"]')
      .contains(/создать продукт/i)
      .click();
    cy.get('input[name="name"]').type('Фотобумага Epson S041706');
    cy.get('textarea[name="description"]').type(
      'Фотобумага Epson S041706 изготовлена крупной компанией.'
    );
    cy.get('input[name="pictures"]').type(
      'https://c.dns-shop.ru/thumb/st1/fit/wm/0/0/b3a42822e0627d983cbbc5f69b1cc913/cb70233c81fa59cc712c5a01ae4be74ab828c7be931d8ed96948cc371cc08f4a.jpg.webp'
    );
    cy.get('input[name="wight"]').type('140');
    cy.get('input[name="price"]').type('5500');
    cy.get('button[type="submit"]')
      .contains(/добавить товар/i)
      .click();
  });

  it('Ищем созданный продукт в каталоге с помощью поля поиска, ставим и убираем лайк у первого продукта', () => {
    cy.wait(1000);
    cy.get('input[id="search"]').type('Фотобумага Epson');
    cy.wait(2000);
    cy.get('button[data-testid="favorite"]').eq(0).click();
    cy.get('svg[class~="text-red-500"]');
    cy.get('button[data-testid="favorite"]').eq(0).click();
  });

  it('Ставим и убираем лайки у 3го и 6го продукта', () => {
    cy.wait(1000);
    cy.get('button[data-testid="favorite"]').eq(2).click();
    cy.get('button[data-testid="favorite"]').eq(5).click();
    cy.get('svg[class~="text-red-500"]');
    cy.get('button[data-testid="favorite"]').eq(2).click();
    cy.get('button[data-testid="favorite"]').eq(5).click();
  });
});
