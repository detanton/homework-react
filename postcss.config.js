const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const tailwindcss = require('tailwindcss');

module.exports = {
  plugins: [
    'postcss-preset-env',
    tailwindcss,
    autoprefixer,
    cssnano({ preset: 'default' })
  ]
};
