export type InputsChangeProfileForm = {
  firstName: string;
  secondName: string;
  about: string;
};
