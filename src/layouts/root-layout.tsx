import React from 'react';
import { Outlet, useNavigate } from 'react-router-dom';

import { Header } from '../components/header';
import { Footer } from '../components/footer/footer';
import {
  accessTokenSelector,
  userSelector
} from '../storage/slices/user/selectors';
import { useAppSelector } from '../hooks/use-app-selector';
import { useLazyGetFavoriteProductsQuery } from '../storage/api/products';

export const RootLayout: React.FC = () => {
  const navigate = useNavigate();
  const accessToken = useAppSelector(accessTokenSelector);
  const { data } = useAppSelector(userSelector);
  const [getFavoriteProducts] = useLazyGetFavoriteProductsQuery();

  React.useEffect(() => {
    if (!accessToken) {
      navigate('/signin');
    }
  }, [accessToken, navigate]);

  React.useEffect(() => {
    if (data) {
      getFavoriteProducts(data?._id || '');
    }
  }, [getFavoriteProducts, data?._id, data]);

  return (
    <>
      <Header />
      <div className="max-w-[960px] mx-auto">
        <Outlet />
      </div>
      <Footer />
    </>
  );
};
