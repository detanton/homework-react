export interface InitialStateCart {
  total: number;
  productsPrice: number;
  productsDiscount: number;
  summary: number;
  cartProducts: Array<{
    id: string;
    name: string;
    count: number;
    price: number;
    discount: number;
    wight: string;
    stock: number;
  }>;
}
