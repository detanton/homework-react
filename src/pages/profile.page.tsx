import React from 'react';

import { withProtection } from '../HOCs/with-protection';
import { Header } from '../components/typography/header';
import { TopNavigation } from '../components/top-navigation';
import { ProfileInfo } from '../components/profile-info';
import { UserActions } from '../components/user-actions';

export const ProfilePage: React.FC = withProtection(() => (
  <section>
    <TopNavigation />
    <Header title="Профиль" as="h1" />
    <div className="flex justify-between">
      <ProfileInfo />
      <UserActions />
    </div>
  </section>
));
