import { createApi } from '@reduxjs/toolkit/query/react';

import { customBaseQuery } from '../../utils/custom-base-query';

export const profileApi = createApi({
  reducerPath: 'profileApi',
  baseQuery: customBaseQuery,
  tagTypes: ['Profile'],
  endpoints: (builder) => ({
    getProfile: builder.query({
      query: () => ({
        url: '/users/me',
        method: 'GET'
      }),
      providesTags: ['Profile']
    }),
    changeProfileInfo: builder.mutation({
      query: (data) => ({
        url: '/users/me',
        method: 'PATCH',
        body: data
      }),
      invalidatesTags: ['Profile']
    })
  })
});

export const { useGetProfileQuery, useChangeProfileInfoMutation } = profileApi;
