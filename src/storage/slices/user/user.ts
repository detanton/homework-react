import { createSlice } from '@reduxjs/toolkit';

import type { InitialStateUser } from './types';

const initialState: InitialStateUser = {
  data: null,
  error: '',
  isLoading: false,
  accessToken: ''
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser: (state, action) => {
      state.data = action.payload.data;
      state.accessToken = action.payload.token;
      state.error = action.payload.error;
    }
  }
});

export const sliceName = 'user';

export const { setUser } = userSlice.actions;

export default userSlice.reducer;
