import React from 'react';

import logo from '../../../public/images/logo.svg';

export const Logo: React.FC = () => (
  <img src={logo} alt="DogFood" className="w-[224px] h-[56px]" />
);
