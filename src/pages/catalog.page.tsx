import React from 'react';

import { withProtection } from '../HOCs/with-protection';
import { Header } from '../components/typography/header';
import { TopNavigation } from '../components/top-navigation';
import { CardList } from '../components/card-list';

export const CatalogPage: React.FC = withProtection(() => (
  <>
    <TopNavigation />
    <Header title="Каталог" as="h1" />
    <CardList />
  </>
));
