import React from 'react';

import { withProtection } from '../HOCs/with-protection';
import { Header } from '../components/typography/header';
import { TopNavigation } from '../components/top-navigation';
import { FavoritesList } from '../components/favorites-list';

export const FavoritesPage: React.FC = withProtection(() => (
  <>
    <TopNavigation />
    <Header title="Избранное" as="h1" />
    <FavoritesList />
  </>
));
