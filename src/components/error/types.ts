export interface ErrorProps {
  title: string;
  needButton?: boolean;
}
