import { faImage } from '@fortawesome/free-regular-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

export const ProfileInfoSceleton: React.FC = () => (
  <section className="flex w-[960px] my-5 animate-pulse">
    <div className="w-28 h-28 rounded-full bg-gray-200 dark:bg-gray-700 flex items-center justify-center">
      <FontAwesomeIcon
        icon={faImage}
        className="fa-3x text-gray-300 dark:bg-gray-700"
      />
    </div>
    <div className="ml-4">
      <div className="bg-gray-200 h-5 w-48 rounded-2xl dark:bg-gray-700" />
      <div className="bg-gray-200 h-3 w-48 mt-2 rounded-md dark:bg-gray-700" />
      <div className="bg-gray-200 h-3 w-48 mt-2 rounded-md dark:bg-gray-700" />
    </div>
  </section>
);
