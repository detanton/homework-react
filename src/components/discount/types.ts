export interface DiscountProps {
  discount: number | undefined;
}
