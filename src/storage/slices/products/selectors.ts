import { createSelector } from '@reduxjs/toolkit';

import type { RootState } from '../../types';

import { sliceName } from './products';

export const productsSelector = createSelector(
  (state: RootState) => state[sliceName].favoritesCount,
  (favoritesCount) => ({ favoritesCount })
);
