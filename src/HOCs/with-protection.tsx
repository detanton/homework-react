import type { ComponentType } from 'react';
import React from 'react';
import { Navigate, useLocation } from 'react-router-dom';

import { useAppSelector } from '../hooks/use-app-selector';
import { accessTokenSelector } from '../storage/slices/user/selectors';
import { getDisplayName } from '../utils/get-display-name';

export const withProtection = <P extends object>(
  WrappedComponent: ComponentType<P>
) => {
  const ReturnedComponent: React.FC<P> = (props) => {
    const accessToken = useAppSelector(accessTokenSelector);
    const { pathname } = useLocation();

    if (!accessToken) {
      return (
        <Navigate
          to="/signin"
          state={{
            from: pathname
          }}
        />
      );
    }

    return <WrappedComponent {...props} />;
  };

  ReturnedComponent.displayName = `withQuery${getDisplayName(
    WrappedComponent
  )}`;

  return ReturnedComponent;
};
