import '@testing-library/jest-dom';
import { describe, expect, it } from '@jest/globals';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import type { createMemoryRouter } from 'react-router-dom';

import { TestWrapperProvider } from '../../../utils/test-wrapper';
import { additionalNavElements, navElements, socialMedia } from '../footer';

describe('Тестирование компонента Footer', () => {
  let router: ReturnType<typeof createMemoryRouter>;

  beforeEach(() => {
    router = TestWrapperProvider({ initialRouterEntires: ['/'] });
  });

  it('Отображается логотип', () => {
    // Поскольку у нас 2 одинаковых изображения логотипа: в header и footer, выбираем второе
    const logoImage = screen.getAllByRole('img', { name: 'DogFood' }).at(1);
    expect(logoImage?.getAttribute('alt')).toContain('DogFood');
  });

  it('Отображается ссылка в первом столбце', () => {
    navElements.forEach((elem, index) => {
      expect(elem).toEqual(navElements[index]);
    });
  });

  it('Отображается ссылка во втором столбце', () => {
    additionalNavElements.forEach((elem, index) => {
      expect(elem).toEqual(additionalNavElements[index]);
    });
  });

  it('Отображается ссылка социальных медиа', () => {
    socialMedia.forEach((elem, index) => {
      expect(elem).toEqual(socialMedia[index]);
    });
  });

  it('Отображается копирайт', () => {
    expect(screen.getByText('© «Интернет-магазин DogFood.ru»')).toBeDefined();
  });

  it('Отображается номер телефона и email', () => {
    expect(screen.getByText('8 (999) 00-00-00')).toBeDefined();
    expect(screen.getByText('dogfood.ru@gmail.com')).toBeDefined();
  });

  it('Переход на страницу Каталог', async () => {
    const favoriteProductsLink = screen.getByTestId('products');

    await userEvent.click(favoriteProductsLink);

    expect(router?.state.location.pathname).toEqual('/products');
  });
});
