import React from 'react';
import type { UseControllerProps } from 'react-hook-form';
import { useController } from 'react-hook-form';

import type { FieldProps, Inputs } from './types';

export const Field = (props: UseControllerProps<Inputs> & FieldProps) => {
  const { field, fieldState } = useController(props);
  const className = `border border-slate-200 outline-none outline-2 outline-offset-0 rounded-lg px-4 py-3 transition ease-in-out w-full duration-300 mt-1 text-base focus:border-yellow-300 ${
    fieldState.error && 'outline-red-500'
  }`;

  return (
    <label className="block text-xs mt-3">
      {props.label}
      <input {...props} {...field} className={className} />
      {fieldState.error && (
        <p className="text-red-500 mt-1">{fieldState.error.message}</p>
      )}
    </label>
  );
};
