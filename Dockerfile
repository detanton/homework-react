FROM node:20-alpine
WORKDIR /app
RUN pwd
COPY . ./
RUN npm install

ENTRYPOINT [ "npm", "start" ]