import { createApi } from '@reduxjs/toolkit/query/react';

import { customBaseQuery } from '../../utils/custom-base-query';
import type { Product } from '../../components/card/types';
import type { CreateProductFormInputs } from '../../components/forms/create-product-form/types';
import { setFavoritesCount } from '../slices/products/products';

import type { Products } from './types';

export const productsApi = createApi({
  reducerPath: 'productsApi',
  baseQuery: customBaseQuery,
  tagTypes: ['Products'],
  endpoints: (builder) => ({
    getProducts: builder.query<Products, { query?: string; page?: number }>({
      query: ({ query, page }) => ({
        url: `/products/${query ? 'search' : ''}`,
        method: 'GET',
        params: {
          page,
          limit: 12,
          query
        }
      }),
      serializeQueryArgs: ({ endpointName, queryArgs: { query } }) =>
        endpointName + query,
      merge: (currentCache, newValue, { arg: { page } }) => {
        if (page === 1) {
          return;
        }

        currentCache.products.push(...newValue.products);
      },
      transformResponse: (response: Products & Array<Product>) => {
        if (response.products) {
          return response;
        }

        return {
          total: response.length,
          products: response
        };
      },
      forceRefetch: ({ currentArg, previousArg }) => currentArg !== previousArg,
      providesTags: (result) => {
        return result
          ? [
              ...result.products.map(({ _id }) => ({
                type: 'Products' as const,
                id: _id
              })),
              { type: 'Products', id: 'PARTIAL-LIST' }
            ]
          : [{ type: 'Products', id: 'PARTIAL-LIST' }];
      }
    }),
    getProduct: builder.query<Product, { id: string }>({
      query: ({ id }) => ({
        url: `/products/${id}`,
        method: 'GET'
      }),
      providesTags: (result) => [{ type: 'Products', id: result?._id }]
    }),
    createProduct: builder.mutation<Product, CreateProductFormInputs>({
      query: (data) => ({
        url: '/products',
        method: 'POST',
        body: data
      }),
      invalidatesTags: (result) => [
        { type: 'Products', id: result?._id },
        { type: 'Products', id: 'PARTIAL-LIST' },
        { type: 'Products', id: 'FAVORITES-LIST' }
      ]
    }),
    removeProduct: builder.mutation<Product, { id: string }>({
      query: ({ id }) => ({
        url: `/products/${id}`,
        method: 'DELETE'
      }),
      invalidatesTags: (result) => [{ type: 'Products', id: result?._id }]
    }),
    getFavoriteProducts: builder.query<Products, string>({
      query: () => ({
        url: '/products',
        method: 'GET'
      }),
      transformResponse: (result: Products, meta, userId) => ({
        ...result,
        products: result.products.filter((product) =>
          product.likes.includes(userId)
        )
      }),
      async onQueryStarted(args, { dispatch, queryFulfilled }) {
        try {
          const { data } = await queryFulfilled;
          const favoriteProducts = data.products.filter((product) =>
            product.likes.includes(args)
          );

          dispatch(setFavoritesCount(favoriteProducts.length));
        } catch (error) {
          dispatch(setFavoritesCount(0));
        }
      },
      providesTags: [{ type: 'Products', id: 'FAVORITES-LIST' }]
    }),
    toggleFavorite: builder.mutation<Product, { id: string; toggle?: boolean }>(
      {
        query: ({ id, toggle }) => ({
          url: `/products/likes/${id}`,
          method: toggle ? 'DELETE' : 'PUT'
        }),
        invalidatesTags: (result) => [
          { type: 'Products', id: result?._id },
          { type: 'Products', id: 'FAVORITES-LIST' }
        ]
      }
    )
  })
});

export const {
  useGetProductsQuery,
  useGetProductQuery,
  useCreateProductMutation,
  useGetFavoriteProductsQuery,
  useLazyGetFavoriteProductsQuery,
  useToggleFavoriteMutation,
  useRemoveProductMutation
} = productsApi;
