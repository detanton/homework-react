import { faBone } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

export const Loader = () => (
  <div className="grid place-content-center h-full">
    <FontAwesomeIcon icon={faBone} size="3x" fixedWidth spin />
  </div>
);
